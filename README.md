# krakend-api-gateway
### setup

install golang [properly][1]  
setup [$GOPATH][2]  
open terminal and enter folowing

```sh
# install dep
go get -u github.com/golang/dep/cmd/dep

# clone the repo in $GOPATH/src/gitlab.com/lakshanwd/krakend-api-gateway
git clone https://gitlab.com/lakshanwd/krakend-api-gateway.git $GOPATH/src/gitlab.com/lakshanwd/krakend-api-gateway

# Go to $GOPATH/src/gitlab.com/lakshanwd/krakend-api-gateway
cd $GOPATH/src/gitlab.com/lakshanwd/krakend-api-gateway

# install dependancies
dep ensure

# run the application
go run app.go
```

### testing  
open browser and go to following urls
- http://localhost:8080/s1
- http://localhost:8080/s2
- http://localhost:8080/s3
- http://localhost:8080/s4

or open terminal and enter the following
```sh
curl http://localhost:8080/s1
curl http://localhost:8080/s2
curl http://localhost:8080/s3
curl http://localhost:8080/s4
```

[1]:https://golang.org/doc/install
[2]:https://github.com/golang/go/wiki/SettingGOPATH